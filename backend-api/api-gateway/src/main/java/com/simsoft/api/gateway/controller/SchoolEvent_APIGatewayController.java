package com.simsoft.api.gateway.controller;

import com.simsoft.api.gateway.service.schoolevent.config.SchoolEventMessageConfig;
import com.simsoft.api.gateway.service.schoolevent.model.SchoolEvent;
import org.springframework.amqp.core.DirectExchange;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RequestMapping(path = "/api/event")
public class SchoolEvent_APIGatewayController {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private DirectExchange findAllEventsExchange;

    @Autowired
    private DirectExchange findEventByIdExchange;

    @Autowired
    private DirectExchange addNewEventExchange;

    @GetMapping()
    public Object findAll() {
        try {
            System.out.println("From controller: ");
            return this.rabbitTemplate.convertSendAndReceive(findAllEventsExchange.getName(), SchoolEventMessageConfig.SCHOOL_EVENT_ROUTING_KEY, "findAll");
        }
        catch (Exception e) {
            System.out.println("From controller: " + e.toString());
            return e.toString();
        }
    }

    @GetMapping(path = "/{id}")
    public Object findById(@PathVariable int id) {
        return this.rabbitTemplate.convertSendAndReceive(findEventByIdExchange.getName(), SchoolEventMessageConfig.FIND_SCHOOL_EVENT_BY_ID_ROUTING_KEY, id);
    }

    @PostMapping()
    public Object save(@RequestBody SchoolEvent schoolEvent) {
        return this.rabbitTemplate.convertSendAndReceive(addNewEventExchange.getName(), SchoolEventMessageConfig.ADD_NEW_SCHOOL_EVENT_ROUTING_KEY, schoolEvent);
    }
}
