package com.simsoft.api.schoolinfo.listener;

import com.simsoft.api.schoolinfo.config.SchoolInfoMessageConfig;
import com.simsoft.api.schoolinfo.model.SchoolInfo;
import com.simsoft.api.schoolinfo.repository.SchoolInfoRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SchoolInfoListener {

    @Autowired
    private SchoolInfoRepository schoolInfoRepository;

    @RabbitListener(queues = SchoolInfoMessageConfig.SCHOOL_INFO_QUEUE)
    public Object findAll() {
        return this.schoolInfoRepository.findAll();
    }

    @RabbitListener(queues = SchoolInfoMessageConfig.FIND_SCHOOL_INFO_BY_ID_QUEUE)
    public Object findById(int id) {
        return this.schoolInfoRepository.findById(id);
    }

    @RabbitListener(queues = SchoolInfoMessageConfig.ADD_NEW_SCHOOL_INFO_QUEUE)
    public Object addNewSchool(SchoolInfo schoolInfo) {
        return this.schoolInfoRepository.save(schoolInfo);
    }

}
