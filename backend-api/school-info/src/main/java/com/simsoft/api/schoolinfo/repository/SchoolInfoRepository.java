package com.simsoft.api.schoolinfo.repository;

import com.simsoft.api.schoolinfo.model.SchoolInfo;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface SchoolInfoRepository extends JpaRepository<SchoolInfo, Integer> {

    public ArrayList<SchoolInfo> findAll();

    public SchoolInfo findById(int schoolId);

    public SchoolInfo save(SchoolInfo schoolInfo);

    public int deleteById(int id);
}
