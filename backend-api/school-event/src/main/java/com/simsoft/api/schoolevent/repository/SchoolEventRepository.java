package com.simsoft.api.schoolevent.repository;

import com.simsoft.api.schoolevent.model.SchoolEvent;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.ArrayList;

public interface SchoolEventRepository extends JpaRepository<SchoolEvent, Integer> {

    public ArrayList<SchoolEvent> findAll();

    public SchoolEvent findById(int id);

    public SchoolEvent save(SchoolEvent schoolEvent);

    public int deleteById(int id);
}
