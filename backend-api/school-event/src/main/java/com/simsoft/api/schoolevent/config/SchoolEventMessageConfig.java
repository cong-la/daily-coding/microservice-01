package com.simsoft.api.schoolevent.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class SchoolEventMessageConfig {

    //  FIND ALL
    public static final String SCHOOL_EVENT_QUEUE = "SCHOOL_EVENT_QUEUE";
    public static final String SCHOOL_EVENT_EXCHANGE = "SCHOOL_EVENT_EXCHANGE";
    public static final String SCHOOL_EVENT_ROUTING_KEY = "SCHOOL_EVENT_ROUTING_KEY";

    //  FIND_BY_ID
    public static final String FIND_SCHOOL_EVENT_BY_ID_QUEUE = "FIND_SCHOOL_EVENT_BY_ID_QUEUE";
    public static final String FIND_SCHOOL_EVENT_BY_ID_EXCHANGE = "FIND_SCHOOL_EVENT_BY_ID_EXCHANGE";
    public static final String FIND_SCHOOL_EVENT_BY_ID_ROUTING_KEY = "FIND_SCHOOL_EVENT_BY_ID_ROUTING_KEY";

    //  ADD NEW SCHOOL
    public static final String ADD_NEW_SCHOOL_EVENT_QUEUE = "ADD_NEW_SCHOOL_EVENT_QUEUE";
    public static final String ADD_NEW_SCHOOL_EVENT_EXCHANGE = "ADD_NEW_SCHOOL_EVENT_EXCHANGE";
    public static final String ADD_NEW_SCHOOL_EVENT_ROUTING_KEY = "ADD_NEW_SCHOOL_EVENT_ROUTING_KEY";

    //  QUEUE
    @Bean
    public Queue findAllEventsQueue() {
        return new Queue(SCHOOL_EVENT_QUEUE);
    }

    @Bean
    public Queue findEventByIdQueue() {
        return new Queue(FIND_SCHOOL_EVENT_BY_ID_QUEUE);
    }

    @Bean
    public Queue addNewEventQueue() {
        return new Queue(ADD_NEW_SCHOOL_EVENT_QUEUE);
    }

    //  EXCHANGE
    @Bean
    public DirectExchange findAllEventsExchange() {
        return new DirectExchange(SCHOOL_EVENT_EXCHANGE);
    }

    @Bean
    public DirectExchange findEventByIdExchange() {
        return new DirectExchange(FIND_SCHOOL_EVENT_BY_ID_EXCHANGE);
    }

    @Bean
    public DirectExchange addNewEventExchange() {
        return new DirectExchange(ADD_NEW_SCHOOL_EVENT_EXCHANGE);
    }

    //  BINDING
    @Bean
    public Binding findAllEventsBinding(Queue findAllEventsQueue, DirectExchange findAllEventsExchange) {
        return BindingBuilder.bind(findAllEventsQueue).to(findAllEventsExchange).with(SCHOOL_EVENT_ROUTING_KEY);
    }

    @Bean
    public Binding findEventByIdBinding(Queue findEventByIdQueue, DirectExchange findEventByIdExchange) {
        return BindingBuilder.bind(findEventByIdQueue).to(findEventByIdExchange).with(FIND_SCHOOL_EVENT_BY_ID_ROUTING_KEY);
    }

    @Bean
    public Binding addNewSchoolEventBinding(Queue addNewEventQueue, DirectExchange addNewEventExchange) {
        return BindingBuilder.bind(addNewEventQueue).to(addNewEventExchange).with(ADD_NEW_SCHOOL_EVENT_ROUTING_KEY);
    }
}
