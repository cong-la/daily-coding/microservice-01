package com.simsoft.api.schoolevent.listener;

import com.simsoft.api.schoolevent.config.SchoolEventMessageConfig;
import com.simsoft.api.schoolevent.model.SchoolEvent;
import com.simsoft.api.schoolevent.repository.SchoolEventRepository;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SchoolEventListener {

    @Autowired
    private SchoolEventRepository schoolEventRepository;

    @RabbitListener(queues = SchoolEventMessageConfig.SCHOOL_EVENT_QUEUE)
    public Object findAll() {
        try {
            System.out.println("From listener: " + this.schoolEventRepository.findAll().toString());
            return this.schoolEventRepository.findAll();
        }
        catch (Exception e) {
            System.out.println("From listener: " + e.toString());
            return e.toString();
        }
    }

    @RabbitListener(queues = SchoolEventMessageConfig.FIND_SCHOOL_EVENT_BY_ID_QUEUE)
    public Object findById(int id) {
        return this.schoolEventRepository.findById(id);
    }

    @RabbitListener(queues = SchoolEventMessageConfig.ADD_NEW_SCHOOL_EVENT_QUEUE)
    public Object addNewEvent(SchoolEvent schoolEvent) {
        try {
            return this.schoolEventRepository.save(schoolEvent);
        }
        catch (Exception e) {
            System.out.println("Error: " + e.toString());
            return e;
        }
    }
}
