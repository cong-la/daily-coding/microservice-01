package com.simsoft.api.schoolevent;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
public class SchoolEventApplication {

	public static void main(String[] args) {
		SpringApplication.run(SchoolEventApplication.class, args);
	}

}
