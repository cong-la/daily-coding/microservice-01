#. For error: ERROR [internal] load metadata for docker.io/library/nginx:1.17.1-alpine
export DOCKER_BUILDKIT=0
export COMPOSE_DOCKER_CLI_BUILD=0

1. Build docker image:
docker build -t frontend .

2. Run docker image:
docker run -p 80:80 --name frontend --net docker-microservice-network frontend
