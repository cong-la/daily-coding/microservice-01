import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup} from "@angular/forms";
import {SchoolEventService} from "../service/school-event.service";

@Component({
  selector: 'app-school-info',
  templateUrl: './school-info.component.html',
  styleUrls: ['./school-info.component.css']
})
export class SchoolInfoComponent implements OnInit {

  newSchoolForm = new FormGroup({
    name: new FormControl(''),
  });


  constructor(private schoolEventService: SchoolEventService) { }

  ngOnInit(): void {
  }

  saveSchool() {
    const name = this.newSchoolForm.get('name')?.value;
    const url = 'http://127.0.0.1:8000/api/school'
    this.schoolEventService.postRequest(url, name)
      .subscribe(
        res => {
          console.log(res)
        },
        error => {}
      );

  }

}
