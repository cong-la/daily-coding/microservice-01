import { TestBed } from '@angular/core/testing';

import { SchoolEventService } from './school-event.service';

describe('SchoolEventService', () => {
  let service: SchoolEventService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(SchoolEventService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
